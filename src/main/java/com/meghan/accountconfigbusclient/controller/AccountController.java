package com.meghan.accountconfigbusclient.controller;


import com.meghan.accountconfigbusclient.model.Account;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RefreshScope
public class AccountController {

    @Value("${limit}")
    private String dailyLimit;

    @Value("${db.port}")
    private String dbPort;

    @Value("${encrypted.value}")
    private String encryptedValue;

    private List<Account> accounts = Stream.of(new Account(1, "saving", new BigDecimal(1000.00)),
            new Account(2, "current", new BigDecimal(5000.00)),
            new Account(3, "saving", new BigDecimal(3000.00)))
            .collect(Collectors.toList());

    @RequestMapping(value = "/accounts/{accountId}", method = RequestMethod.GET)
    public @ResponseBody Account getAccount(@PathVariable String accountId) {

        accounts.forEach(account -> account.setDailyLimit(dailyLimit));

        Optional<Account> optionalAccount = accounts
                .stream()
                .filter(account -> Integer.parseInt(accountId) == account.getAccountId())
                .findFirst();

        Account acc;
        if (optionalAccount.isPresent()) {
            acc = optionalAccount.get();
            acc.setDbPort(dbPort);
            acc.setDecryptedValue(encryptedValue);
        } else {
            acc = new Account(0, "default account", new BigDecimal(0.0));
        }

        return acc;
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public @ResponseBody List<Account> getAllAccounts() {
        accounts.forEach(account -> account.setDailyLimit(dailyLimit));
        return accounts;
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.POST)
    public @ResponseBody List<Account> addAccount(@RequestBody Account account) {
        accounts.forEach(acc -> acc.setDailyLimit(dailyLimit));
        accounts.add(account);
        return accounts;
    }

    @RequestMapping(value = "/accounts/{accountId}", method = RequestMethod.DELETE)
    public @ResponseBody List<Account> deleteAccount(@PathVariable String accountId) {

        accounts.forEach(acc -> acc.setDailyLimit(dailyLimit));
        accounts.removeIf(account -> Integer.parseInt(accountId) == account.getAccountId());
        return accounts;
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.PUT)
    public @ResponseBody List<Account> updateAccount(@RequestBody Account acc) {

        accounts.forEach(acct -> acct.setDailyLimit(dailyLimit));
        accounts.removeIf(account -> acc.getAccountId() == account.getAccountId());
        accounts.add(acc);
        return accounts;
    }
}


