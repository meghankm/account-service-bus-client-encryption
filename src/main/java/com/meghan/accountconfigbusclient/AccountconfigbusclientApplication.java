package com.meghan.accountconfigbusclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountconfigbusclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountconfigbusclientApplication.class, args);
	}

}
